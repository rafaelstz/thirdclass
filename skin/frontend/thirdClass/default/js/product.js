/**
    The MIT License (MIT)
    
    Copyright (c) 2013 Rick Benetti - WDSIGN
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 * @project_name thirdClass
 * @copyright    Copyright (c) 2013 Rick Benetti - WDSIGN - thirdClass (http://thirdclass.wdsign.com.br)
 * @author       Rick Benetti <rick@wdsign.com.br>
**/
jQuery(function() {
    // function call smart_Tabs
	jQuery('.tabs-product')._esmart_tabs();
    // function call bxSlider for upsell
    jQuery('#upsell-product-table').bxSlider({
        slideheight: 320,
        slideWidth: 760,
        minSlides: 5,
        maxSlides: 5,
        moveSlides: 1,
        pager: false,
        slideMargin: 0
    });
    // function call bxSlider for more view images products
	jQuery('.more-views ul').bxSlider({
		slideheight: 92,
		slideWidth: 92,
		minSlides: 3,
		maxSlides: 3,
		moveSlides: 1,
		pager: false
	});
});
