/**
    The MIT License (MIT)
    
    Copyright (c) 2013 Rick Benetti - WDSIGN
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 * @project_name thirdClass
 * @copyright    Copyright (c) 2013 Rick Benetti - WDSIGN - thirdClass (http://thirdclass.wdsign.com.br)
 * @author       Rick Benetti <rick@wdsign.com.br>
**/
/**
*
* Troca de tipo de formulario
* @author: Rick Benetti -  <rick@wdsign.com.br>
*
**/
jQuery(document).ready(function() {
    var nome              = jQuery('.form-item-name'),
        cpf               = jQuery('.form-item-cpf'),
        aniversario       = jQuery('.form-item-dob'),
        sexo              = jQuery('.form-item-gender'),
        registro_geral    = jQuery('.form-item-rg'),
        isento            = jQuery('.form-item-isento');

    jQuery(cpf).find('label').html('<sup>*</sup>CPF').addClass('required');
    jQuery(cpf).find('input').attr('maxlenght','14').attr('onkeydown','javascript:return aplicaMascaraTaxvat(this,14,event)').attr('onkeyup','javascript:return aplicaMascaraTaxvat(this,14,event)');
    jQuery(isento).hide();
    jQuery(".select-type-form").on('click', function() {
        var value = jQuery(this).attr('id');
        switch(value){
            case 'type-individual':
                //console.log('Ativado para type-individual');
                jQuery(nome).find('.field-firstname label').html('<sup>*</sup>Nome');
                jQuery(nome).find('.field-lastname label').html('<sup>*</sup>Sobrenome');
                jQuery(cpf).find('label').html('<sup>*</sup>CPF').addClass('required');
                jQuery(cpf).find('input').attr('maxlenght','14').attr('onkeydown','javascript:return aplicaMascaraTaxvat(this,14,event)').attr('onkeyup','javascript:return aplicaMascaraTaxvat(this,14,event)');
                jQuery(registro_geral).find('> label').html('RG').parent().removeClass('form-item-inscricao-estadual');
                jQuery(registro_geral).find('.form-item-isento').hide();
                break;
            case 'type-company':
                //console.log('Ativado para type-company');
                jQuery(nome).find('.field-firstname label').html('<sup>*</sup>Razão Social');
                jQuery(nome).find('.field-lastname label').html('<sup>*</sup>Nome Fantasia');
                jQuery(cpf).find('label').html('<sup>*</sup>CNPJ').addClass('required');
                jQuery(cpf).find('input').attr('maxlenght','18').attr('onkeydown','javascript:return aplicaMascaraTaxvat(this,18,event)').attr('onkeyup','javascript:return aplicaMascaraTaxvat(this,18,event)');
                jQuery(isento).show().find('.chk-isento').on('click', function() {
                    jQuery('#rg').attr('disabled', true).attr('readonly', true).attr('value','Isento').addClass('disable-input');
                });
                jQuery(sexo).hide();
                jQuery(aniversario).hide();
                jQuery(registro_geral).find('>label').html('Inscrição Estadual').parent().addClass('form-item-inscricao-estadual');
                break;
        }
    });
});
/**
*
* CEP autocomplete address
*
**/
jQuery(document).ready(function() {
    jQuery('.postcode').blur(function() {

        var cep = jQuery('.postcode').val();
        var $icon = jQuery('.icon-loading');
        var $icon2 = jQuery('.icon-zip');
        jQuery($icon).show();
        jQuery($icon2).hide();
        jQuery.ajax({
            url: '/checkout/ajax/getAddressByPostcode',
            data: {
                cep: cep
            },
            complete: function(data) {
                data = JSON.parse(data.responseText);
                if (data != null) {
                    jQuery($icon).hide();
                    jQuery('.street').val(data.tipo_logradouro + ' ' + data.logradouro).show();
                    jQuery('.bairro').val(data.bairro).show();
                    jQuery('.city').val(data.cidade).show();

                    jQuery('.region').val(data.uf).show();

                    var $region = jQuery('#region_id');

                    switch (data.uf) {
                        case 'AC':
                            jQuery($region).val(485);
                            break;
                        case 'AL':
                            jQuery($region).val(486);
                            break;
                        case 'AP':
                            jQuery($region).val(487);
                            break;
                        case 'AM':
                            jQuery($region).val(488);
                            break;
                        case 'BA':
                            jQuery($region).val(489);
                            break;
                        case 'CE':
                            jQuery($region).val(490);
                            break;
                        case 'DF':
                            jQuery($region).val(491);
                            break;
                        case 'ES':
                            jQuery($region).val(492);
                            break;
                        case 'GO':
                            jQuery($region).val(493);
                            break;
                        case 'MA':
                            jQuery($region).val(494);
                            break;
                        case 'MT':
                            jQuery($region).val(495);
                            break;
                        case 'MS':
                            jQuery($region).val(496);
                            break;
                        case 'MG':
                            jQuery($region).val(497);
                            break;
                        case 'PA':
                            jQuery($region).val(498);
                            break;
                        case 'PB':
                            jQuery($region).val(499);
                            break;
                        case 'PR':
                            jQuery($region).val(500);
                            break;
                        case 'PE':
                            jQuery($region).val(501);
                            break;
                        case 'PI':
                            jQuery($region).val(502);
                            break;
                        case 'RJ':
                            jQuery($region).val(503);
                            break;
                        case 'RN':
                            jQuery($region).val(504);
                            break;
                        case 'RS':
                            jQuery($region).val(505);
                            break;
                        case 'RO':
                            jQuery($region).val(506);
                            break;
                        case 'RR':
                            jQuery($region).val(507);
                            break;
                        case 'SC':
                            jQuery($region).val(508);
                            break;
                        case 'SP':
                            jQuery($region).val(509);
                            break;
                        case 'SE':
                            jQuery($region).val(510);
                            break;
                        case 'TO':
                            jQuery($region).val(511);
                            break;
                    }
                }
            }
        });
    });
});

//Aplica a máscara no campo
//Função para ser utilizada nos eventos do input para formatação dinâmica
function aplicaMascaraTaxvat(campo,tammax,teclapres) {
    var tecla = teclapres.keyCode;

    if ((tecla < 48 || tecla > 57) && (tecla < 96 || tecla > 105) && tecla != 46 && tecla != 8) {
        return false;
    }

    var vr = campo.value;
    vr = vr.replace( /\//g, "" );
    vr = vr.replace( /-/g, "" );
    vr = vr.replace( /\./g, "" );
    var tam = vr.length;

    if ( tam <= 2 ) {
        campo.value = vr;
    }
    if ( (tam > 2) && (tam <= 5) ) {
        campo.value = vr.substr( 0, tam - 2 ) + '-' + vr.substr( tam - 2, tam );
    }
    if ( (tam >= 6) && (tam <= 8) ) {
        campo.value = vr.substr( 0, tam - 5 ) + '.' + vr.substr( tam - 5, 3 ) + '-' + vr.substr( tam - 2, tam );
    }
    if ( (tam >= 9) && (tam <= 11) ) {
        campo.value = vr.substr( 0, tam - 8 ) + '.' + vr.substr( tam - 8, 3 ) + '.' + vr.substr( tam - 5, 3 ) + '-' + vr.substr( tam - 2, tam );
    }
    if ( (tam == 12) ) {
        campo.value = vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '/' + vr.substr( tam - 6, 4 ) + '-' + vr.substr( tam - 2, tam );
    }
    if ( (tam > 12) && (tam <= 14) ) {
        campo.value = vr.substr( 0, tam - 12 ) + '.' + vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '/' + vr.substr( tam - 6, 4 ) + '-' + vr.substr( tam - 2, tam );
    }
}

//Verifica se CPF ou CGC e encaminha para a devida função, no caso do cpf/cgc estar digitado sem mascara
function verificaCpfCnpj(cpf_cnpj) {
    if (cpf_cnpj.length == 11) {
        return(verifica_cpf(cpf_cnpj));
    } else if (cpf_cnpj.length == 14) {
        return(verifica_cnpj(cpf_cnpj));
    } else {
        return false;
    }
    return true;
}

//Verifica se o número de CPF informado é válido
function verificaCpf(sequencia) {
    if ( Procura_Str(1,sequencia,'00000000000,11111111111,22222222222,33333333333,44444444444,55555555555,66666666666,77777777777,88888888888,99999999999,00000000191,19100000000') > 0 ) {
        return false;
    }
    seq = sequencia;
    soma = 0;
    multiplicador = 2;
    for (f = seq.length - 3;f >= 0;f--) {
        soma += seq.substring(f,f + 1) * multiplicador;
        multiplicador++;
    }
    resto = soma % 11;
    if (resto == 1 || resto == 0) {
        digito = 0;
    } else {
        digito = 11 - resto;
    }
    if (digito != seq.substring(seq.length - 2,seq.length - 1)) {
        return false;
    }
    soma = 0;
    multiplicador = 2;
    for (f = seq.length - 2;f >= 0;f--) {
        soma += seq.substring(f,f + 1) * multiplicador;
        multiplicador++;
    }
    resto = soma % 11;
    if (resto == 1 || resto == 0) {
        digito = 0;
    } else {
        digito = 11 - resto;
    }
    if (digito != seq.substring(seq.length - 1,seq.length)) {
        return false;
    }
    return true;
}

//Verifica se o número de CNPJ informado é válido
function verificaCnpj(sequencia) {
    seq = sequencia;
    soma = 0;
    multiplicador = 2;
    for (f = seq.length - 3;f >= 0;f-- ) {
        soma += seq.substring(f,f + 1) * multiplicador;
        if ( multiplicador < 9 ) {
            multiplicador++;
        } else {
            multiplicador = 2;
        }
    }
    resto = soma % 11;
    if (resto == 1 || resto == 0) {
        digito = 0;
    } else {
        digito = 11 - resto;
    }
    if (digito != seq.substring(seq.length - 2,seq.length - 1)) {
        return false;
    }

    soma = 0;
    multiplicador = 2;
    for (f = seq.length - 2;f >= 0;f--) {
        soma += seq.substring(f,f + 1) * multiplicador;
        if (multiplicador < 9) {
            multiplicador++;
        } else {
            multiplicador = 2;
        }
    }
    resto = soma % 11;
    if (resto == 1 || resto == 0) {
        digito = 0;
    } else {
        digito = 11 - resto;
    }
    if (digito != seq.substring(seq.length - 1,seq.length)) {
        return false;
    }
    return true;
}


//Procura uma string dentro de outra string
function procuraStr(param0,param1,param2) {
    for (a = param0 - 1;a < param1.length;a++) {
        for (b = 1;b < param1.length;b++) {
            if (param2 == param1.substring(b - 1,b + param2.length - 1)) {
                return a;
            }
        }
    }
    return 0;
}

//Retira a máscara do valor de cpf_cnpj
function retiraMmascara(cpf_cnpj) {
    return cpf_cnpj.replace(/\./g,'').replace(/-/g,'').replace(/\//g,'')
}

// Formata o CEP
function formataCEP(Campo, teclapres) {
    var tecla = teclapres.keyCode;
    var vr = new String(Campo.value);
    vr = vr.replace("-", "");
    tam = vr.length + 1;
    if (tecla != 8)
    {
        if (tam == 6)
            Campo.value = vr.substr(0, 5) + '-' + vr.substr(5, 5);
    }
}
