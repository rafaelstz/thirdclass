thirdClass
==========

Magento Package based on [firstClass][1] framework

## Como Instalar o thirdClass
Clonar o repositório no seu localhost:
```git clone git@github.com:WDSIGN/thirdClass.git```

[Baixar o pacote][20] tar.gz ou zip da Magento Commerce versão 1.8.1.0.
Colar todos os arquivos do magento sobre os arquivos do `thirdClass`, alterar o arquivo app/etc/local.xml conforme exemplo abaixo a partir da linha 41:
```
<default_setup>
    <connection>
        <host><![CDATA[localhost]]></host>
        <username><![CDATA[root]]></username>
        <password><![CDATA[]]></password>
        <dbname><![CDATA[wdsign_thirdclass]]></dbname>
        <initStatements><![CDATA[SET NAMES utf8]]></initStatements>
        <model><![CDATA[mysql4]]></model>
        <type><![CDATA[pdo_mysql]]></type>
        <pdoType><![CDATA[]]></pdoType>
        <active>1</active>
    </connection>
</default_setup>
```

## Instalar banco de dados modelo
Descompactar arquivo wdsign_thirdclass.tar.gz fora do diretório do projeto:
```
tar -zxvf wdsign_thirdclass.sql.tar.gz "C:\Users\SeuUsuario\Documents\dumps"
```
Para importar a base de dados pode-se utilizar um aplicativo de sua preferência ou o comando abaixo se o mysql estiver devidamente configurado:
```
mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL -e "DROP DATABASE IF EXISTS DB_LOCAL;"
mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -e "CREATE DATABASE DB_LOCAL;"

mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL < ARQUIVO.sql

mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL -e "update core_config_data set value='URL_LOCAL/' where path='web/unsecure/base_url';"
mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL -e "update core_config_data set value='URL_LOCAL/' where path='web/secure/base_url';"

mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL -e "update core_config_data set value='{{unsecure_base_url}}' where path='web/unsecure/base_link_url';"
mysql -h HOST_LOCAL -u USER_LOCAL -pPASS_LOCAL -D DB_LOCAL -e "update core_config_data set value='{{secure_base_url}}' where path='web/secure/base_link_url';"
```
Lembrando que os dados, podem ser removidos dos comandos acima se sua base for na sua máquina em localhost e você não utilizar uma senha para testes:
```
-h HOST_LOCAL
-pPASS_LOCAL
```

## Criando uma base de dados
Caso você não tenha ainda criado sua base para o WDSIGN - thirdClass, recomendamos os passos abaixo:
```
mysql -h HOST_LOCAL -u ROOT_LOCAL -pROOT_PASSWORD_LOCAL -e "CREATE DATABASE DB_LOCAL;"
mysql -h HOST_LOCAL -u ROOT_LOCAL -pROOT_PASSWORD_LOCAL -e "CREATE USER 'USER_LOCAL'@'HOST_LOCAL' IDENTIFIED BY 'PASS_LOCAL';"
mysql -h HOST_LOCAL -u ROOT_LOCAL -pROOT_PASSWORD_LOCAL -e "GRANT ALL ON DB_LOCAL.* TO 'USER_LOCAL'@'HOST_LOCAL';"
```

## Configurando o VirtualHosts
No arquivo hosts do seu windows, mac ou linux o caminho a seguir deverá ser configurado:
```
127.0.0.1 wdsign.thirdclass.dev
```
Se você usa MAMP, XAMPP ou linux, terá um arquivo para edição do seu virtualhost o exemplo abaixo é para usuário windows com XAMPP:
```
<VirtualHost *:80>
    ServerAdmin webmaster@wdsign.dev
    DocumentRoot "C:/xampp/htdocs/wdsign/thirdclass"
    ServerName wdsign.thirdclass.dev
    ErrorLog "logs/wdsign.thirdclass.dev-error.log"
    CustomLog "logs/wdsign.thirdclass.dev-access.log" common
</VirtualHost>
```

## Acesso ao admin
Por ter o hábito de nunca utilizar usuários admin e nem acessos admin o caminho para o admin e o usuário padrão foram modificados:
```
http://wdsign.thirdclass.dev/painel
usuário: wdsign
senha: wdsignbr
```

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)

[1]:https://github.com/WDSIGN/firstClass
[20]:http://www.magentocommerce.com/download
